# Carlo's Cheatsheet

Cheatsheet of custom keymappings and aliases for tmux, vim and zsh

## tmux

### Notes

My tmux config is based on the idea that I should be able to use vim keybindings to control tmux 
behavior, except in cases where something else is more intuitive. Anything not specified here 
uses default tmux keybindings.

### global

+ `C-\`   PREFIX
+ `PREFIX r`      reload tmux config

### sessions

+ `PREFIX C-s`      save session
+ `PREFIX C-r`      restore session

### windows

+ `PREFIX |`    split window horizontally
+ `PREFIX -`    split window vertically
+ `PREFIX TAB`  switch to previous window
+ `PREFIX T`    swap current window to be first
+ `PREFIX C-h`  cycle left
+ `PREFIX C-l`  cycle right
+ `PREFIX q`    kill window

### panes

+ `PREFIX h`    select left pane
+ `PREFIX l`    select right pane
+ `PREFIX j`    select pane above
+ `PREFIX k`    select pane below
+ `PREFIX H`    resize left pane
+ `PREFIX L`    resize right pane
+ `PREFIX J`    resize pane above
+ `PREFIX K`    resize pane below

## vim

+ `:Obsess`  start recording a vim session to file
+ `:Obsess!` stop recording vim session and throw out existing saved session

## zsh

### Notes

My zsh config is set up to use vim keybindings, instead of emacs. All usual vim keybindings apply 
to inline command editing, including modal editing commands.

### environment

+ `reload` source shell profile (`source ~/.zshrc`)
+ `_`      sudo
+ `please` prettier `sudo`

### directories

+ `pu`     push directory (`pushd`)
+ `po`     pop directory (`popd`)
+ `...`    change two directories up (`cd ../..`)
+ `-`      go home (`cd -`)
+ `g`      search in this dir, case insensitive, output line number of found expression (`grep -in`)
+ `recp` recursive copy, preserving dirs - takes (1) a file with a list of dirs and (2) a destination
+ `cdlf`   change into a directory and output a full listing, ordered by name, annotate filetypes
+ `cdlt`   change into a directory and output a full listing, ordered by touch time
+ `take`   create a new directory and change into it
+ `l`      list all files, long format (`ls -la`)
+ `ll`     list non-hidden files, long format (`ls -l`)
+ `la`     list all files, except for `.` and `..`, in long format (`ls -lA`)
+ `sl`     correct common typo (`ls`)
+ `lsa`    list all files in long format using unit suffixes (`ls -lah`)
+ `lf`     list all files, long format, with blocks, with unit suffixes, sort by name, annotate filetypes
+ `lt`     list all files, long format, with blocks, with unit suffixes, sort by touch time

### mercurial

+ `clone`  clone given repo; takes a repo url
+ `commit` commit staged changes; takes a message string; accepts additional args
+ `status` show current local repo status
+ `push`   push committed changes
+ `branch` show the current branch; if provided a branch name, creates a new branch; if pluralized, shows all branches
+ `tip`    show `hg tip` output
+ `out`    show committed changes to be pushed
+ `merge`  merge branch; takes usual args
+ `pull`   pull changes
+ `revert` undo uncommitted changes; takes a file or glob pattern
+ `revall` undo all uncommitted changes unconditionally
+ `hd`     show mercurial diff info; takes optional file
+ `hup`    pull and update
+ `upto`   update to supplied branch
+ `parents` show all parents of the current branch
+ `automerge` run pistrang's merge forward tool

### python+django

+ `tags`   generate ctags of python files for going to definition in vim
+ `getbusy` run server with pudb graphical debugger
+ `pydef`  show class and function definitions in provided file
+ `pydefs` show class and function definitions in all files in current dir (recursive)
+ `pyloc`  python loc in current directory (recursive), including migrations
+ `pycounts` python loc in current directory (recursive), excluding migrations

### ssh

+ `qa`     ssh to qa
+ `semi`   ssh to semi
+ `east`   ssh to east
+ `west`   ssh to west
+ `ord`    ssh to ord

### cstar_gulp testing

+ `cstar`  run this projects cstar module with provided args
+ `buildit` run cstar tasks in the order that jenkins runs them for econsumer

