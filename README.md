# Mac Setup

My notes on setting up a new Mac dev environment, minimum necessary to get productive

## Install via download
  - [Anaconda](https://www.continuum.io/downloads)
  - [Chrome Canary](https://www.google.com/chrome/browser/canary.html)
  - [Docker for Mac](https://docs.docker.com/docker-for-mac/)
  - [Firefox Developer Edition](https://www.mozilla.org/en-US/firefox/developer/)
  - [Dracula for iTerm2](https://draculatheme.com/iterm/)
  - [Dracula for Terminal.app](https://draculatheme.com/terminal/)
  - [iTerm2](https://www.iterm2.com/)
  - [Hipchat](https://www.hipchat.com/downloads)
  - [ImageOptim](https://imageoptim.com/mac)
  - [Kaleidoscope](http://www.kaleidoscopeapp.com/)
  - [JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
  - [LastPass](https://lastpass.com/misc_download2.php)
  - [Postgress.app](https://postgresapp.com/)

## Install via App Store
  - [Evernote](https://evernote.com/)
  - [Magnet](http://magnet.crowdcafe.com/)
  - [Paw](https://paw.cloud/)
  - [Postico](https://eggerapps.at/postico/)
  - [Slack](https://slack.com/downloads/osx)
  - [Spark](https://sparkmailapp.com/)
  - [Taurine](http://taurine.unbelievableproductivity.com/)
  - [XCode](https://developer.apple.com/xcode/)
  
### Optional packages

  - [Clean My Mac](http://macpaw.com/cleanmymac)
  - [R](http://lib.stat.cmu.edu/R/CRAN/)
  - [R Studio](https://www.rstudio.com/products/rstudio/download3/)
  - [Visual Studio Code](https://code.visualstudio.com/download)
  - [XQuartz](http://www.xquartz.org/)


## Install via shell

### Homebrew

```bash
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

### Packages via homebrew

```bash
brew update; brew install automake xz ag; brew install tmux zsh vim git git-flow mercurial lastpass-cli awscli rbenv rbenv-gemset ruby-build htop httpie parallel tree reattach-to-user-namespace lastpass-cli awscli doxygen roswell opam yarn; brew cask install lumen
```

#### Configure lastpass-cli and awscli

### Ruby via rbenv

```bash
rbenv install <latest>; rbenv global <latest>
```

### Node+NPM via NVM
```bash
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash
nvm install 8.1.3; nvm alias default 8.1.3 // install current production node version
```

### Reason via OPAM
```bash
opam init
opam update
opam switch 4.02.3
eval `opam config env`
opam install reason
```

### SBCL via roswell
```bash
ros install sbcl
```

## Set up zsh, vim + tmux

#### Set up SSH keys, then...

### Powerline Fonts
```bash
git clone https://github.com/powerline/fonts.git ~/.powerline; pushd ~/.powerline; ./install.sh; popd
```

### [Prezto from fork](https://gitlab.com/carlodicelico/prezto)

```bash
cp .zshrc .zshrc.old
git clone --recursive https://gitlab.com/carlodicelico/prezto.git "${ZDOTDIR:-$HOME}/.zprezto";
setopt EXTENDED_GLOB
for rcfile in "${ZDOTDIR:-$HOME}"/.zprezto/runcoms/^README.md(.N); do
  ln -s "$rcfile" "${ZDOTDIR:-$HOME}/.${rcfile:t}"
done
chsh -s /bin/zsh
cd .zprezto && git remote add upstream https://github.com/sorin-ionescu/prezto.git
git fetch upstream && git merge upstream/master
# copy changes in .zshrc.old to current .zshrc, then
git commit -am 'update .zshrc' && git push -u origin master
```

### Tmux plugin manager

```bash
$ git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
```

### SPF-13 Vim

```bash
curl https://j.mp/spf13-vim3 -L > spf13-vim.sh && sh spf13-vim.sh
```

### Custom configs

```bash
# Clone & zsh, tmux, and vim and install tmux plugins
git clone https://gitlab.com/carlodicelico/dotfiles.git ~/Source/dotfiles
yes | cp -f ~/Source/dotfiles/{.tmux.conf,.vimrc.before.local,.vimrc.bundles.local,.vimrc.local} ~
vim +BundleInstall! +BundleClean +q
PREFIX + I
```

Install VSCode settings via the Sync Settings extension and your saved settings gist.

![zsh+tmux+vim with dracula colors (flux disabled), sauce code pro & powerline fonts and truecolor support](https://gitlab.com/carlodicelico/dotfiles/raw/master/iTerm+tmux+vim+zsh.png)
##### zsh+tmux+vim with dracula colors (flux disabled), sauce code pro & powerline fonts and truecolor support

![VSCode with Dracula theme and Material Icon theme](https://gitlab.com/carlodicelico/dotfiles/raw/master/vscode.png)
##### VSCode with Dracula theme and Material Icon theme
